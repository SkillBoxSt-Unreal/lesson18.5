#include <iostream>
#include <string>

class PersonalScore
{public:
	std::string PlayerName = "Jon Dow";
	int PlayerScore = 0;
};

void BubbleSort(PersonalScore* Arr,int* ArrLength) 
{
	bool b = true;
	while (b)
	{
		b = false;
		int n = 0;
		int LastElm = *ArrLength;
		for (int i = n; i < LastElm; i++)
		{
			if (Arr[i].PlayerScore < Arr[i + 1].PlayerScore)
			{
				std::swap(Arr[i], Arr[i + 1]);
				b = true;
			}
		}
		n++;
	}
}

void PrintWinScore(PersonalScore* Arr, int* ArrLength)
{
	std::cout << "\nScoreList :\n";
	int LastElm = *ArrLength;
	for (int i = 0; i < LastElm; ++i)
	{
		std::cout << "Player N." << i << " : " << Arr[i].PlayerName << " with score - " << Arr[i].PlayerScore << '\n';
	}
}

int main() 
{
	int NumberOfPlayers;
	std::cout << "Write Number of Players: ";
	std::cin >> NumberOfPlayers;
	PersonalScore* PlList = new PersonalScore[NumberOfPlayers];
	for (int i=0;i< NumberOfPlayers;i++)
	{
		std::cout << "Player No."<< i <<"Name : ";
		std::cin >> PlList[i].PlayerName;
		std::cout << "\nPlayer Score : ";
		std::cin >> PlList[i].PlayerScore;
	}	

	BubbleSort(PlList,&NumberOfPlayers);
	PrintWinScore(PlList, &NumberOfPlayers);
	delete[] PlList;
	
}